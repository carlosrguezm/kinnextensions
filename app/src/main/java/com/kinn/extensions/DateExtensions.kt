package com.kinn.extensions

import java.util.*

fun Int.toDate(): Date = Date(this.toLong() * 1000L)

fun Long.toDate(): Date = Date(this * 1000L)