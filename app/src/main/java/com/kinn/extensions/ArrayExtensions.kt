package com.kinn.extensions

import java.util.*

/*
ArrayList
 */
/**
 * Limpia una colección e inserta nuevos elementos
 * @receiver ArrayList<T>
 * @param items List<T>
 * @return ArrayList<T>
 */
fun <T : Any> ArrayList<T>.refreshList(items: List<T>): ArrayList<T> {
    this.clear()
    this.addAll(items)
    return this
}

/**
 * Inserta nuevos elementos sólo si no existen ya previamente en la lista
 * @receiver ArrayList<T>
 * @param items List<T>
 * @return ArrayList<T>
 */
fun <T : Any> ArrayList<T>.addOnlyNewItems(items: List<T>): ArrayList<T> {
    items.forEach { if (!this.contains(it)) this.add(it) }
    return this
}