package com.kinn.extensions

import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import androidx.core.util.Pair
import androidx.lifecycle.ViewModel


fun ViewModel.downloadFile(context: Context, fileName: String, uriString: String): Pair<Long, String> {
    val destination = String.format("%s/%s",
        context.getExternalFilesDir(null).toString(),
        fileName
    )
    val uri: Uri = Uri.parse("file://$destination")
    val request = DownloadManager.Request(Uri.parse(uriString))
    request.setTitle(fileName)
    request.setDestinationUri(uri)
    request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)

    val manager = context.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
    return Pair(manager!!.enqueue(request), destination)
}